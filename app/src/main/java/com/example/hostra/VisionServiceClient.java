package com.example.hostra;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Hostra on 2/24/2016.
 */
public interface VisionServiceClient {

    public Ocr recognizeText(String url, String languageCode, boolean detectOrientation) throws VisionServiceException;

    public Ocr recognizeText(InputStream stream, String languageCode, boolean detectOrientation) throws VisionServiceException, IOException;

}
