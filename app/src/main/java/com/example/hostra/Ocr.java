package com.example.hostra;

import java.util.List;

/**
 * Created by Hostra on 2/24/2016.
 */
public class Ocr {
    public boolean isAngleDetected;

    public float textAngle;

    public String orientation;

    public String language;

    public List<Regions> regions;
}
