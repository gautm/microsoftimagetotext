package com.example.hostra;

import org.apache.http.client.methods.HttpPost;

/**
 * Created by Hostra on 2/24/2016.
 */
public class HttpPatch extends HttpPost {

    private static final String METHOD_PATCH = "PATCH";

    public HttpPatch(final String url) {
        super(url);
    }

    @Override
    public String getMethod() {
        return METHOD_PATCH;
    }
}
