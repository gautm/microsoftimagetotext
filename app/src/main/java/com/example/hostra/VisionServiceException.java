package com.example.hostra;

import com.google.gson.Gson;

/**
 * Created by Hostra on 2/24/2016.
 */
public class VisionServiceException extends Exception {

    public VisionServiceException(String message) {
        super(message);
    }

    public VisionServiceException(Gson errorObject) {
        super(errorObject.toString());
    }
}
