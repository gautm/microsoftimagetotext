package com.example.hostra;

import java.util.List;

/**
 * Created by Hostra on 2/24/2016.
 */
public class Line {

    public boolean isVertical;

    public List<Word> words;

    public String boundingBox;
}
