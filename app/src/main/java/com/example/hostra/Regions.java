package com.example.hostra;

import java.util.List;

/**
 * Created by Hostra on 2/24/2016.
 */
public class Regions {

    public String boundingBox; //e.g. "boundingBox":"27, 33, 398, 51"

    public List<Line> lines;
}
